package ru.olegsvs.pokemons.data.model.pokemon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.olegsvs.pokemons.common.BaseItem
import java.util.*

@Entity(tableName = "pokemons")
data class Pokemon(
    @PrimaryKey
    @ColumnInfo(name = "pokemonId")
    val id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "pokemonName")
    val name: String,
    @ColumnInfo(name = "pokemonAvatar")
    val avatar: String,
    @ColumnInfo(name = "pokemonDesc")
    val desc: String = "",
    @ColumnInfo(name = "pokemonElement")
    val element: Element
) : BaseItem()


