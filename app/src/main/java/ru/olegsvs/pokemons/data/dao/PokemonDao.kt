package ru.olegsvs.pokemons.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon


@Dao
interface PokemonDao {
    @Query("SELECT * FROM pokemons WHERE pokemonId = :id")
    fun getPokemonById(id: String): LiveData<Pokemon>

    @Query("select * from pokemons order by pokemonElement")
    fun getPokemonsLive(): LiveData<List<Pokemon>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemon(pokemon: Pokemon)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemons(pokemons: List<Pokemon>)

    @Query("DELETE FROM pokemons")
    fun deleteAllPokemons()
}