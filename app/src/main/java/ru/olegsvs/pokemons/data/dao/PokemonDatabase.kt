package ru.olegsvs.pokemons.data.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import ru.olegsvs.pokemons.data.model.pokemon.Element
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import java.util.concurrent.Executors

@Database(entities = [Pokemon::class], version = 3)
@TypeConverters(ElementConverter::class)
abstract class PokemonDatabase : RoomDatabase() {

    abstract fun pokemonDao(): PokemonDao

    companion object {

        @Volatile
        private var INSTANCE: PokemonDatabase? = null

        fun getInstance(context: Context): PokemonDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                PokemonDatabase::class.java, "Pokemons.db"
            )
                .fallbackToDestructiveMigration()
                .addCallback(initPokemons(context))
                .build()

        private fun initPokemons(context: Context): Callback {
            return object : Callback() {
                override fun onDestructiveMigration(db: SupportSQLiteDatabase) {
                    initData(context)
                }

                override fun onCreate(db: SupportSQLiteDatabase) {
                    initData(context)
                }
            }
        }

        private fun initData(context: Context) {
            Executors.newSingleThreadScheduledExecutor()
                .execute(Runnable {
                    getInstance(context).pokemonDao().insertPokemons(
                        listOf(
                            Pokemon(
                                name = "Pikachu",
                                desc = "Pikachu (Пикачу) – пожалуй, самый популярный и наиболее интересный покемон всей империи Pokemon Go. Представляет собой первый тип покемонов электрического типа. Его неспроста считают настоящим талисманом всей игровой вселенной.По внешнему виду это небольшой зверек с желтым мехом и полноватым телосложением. Отличается красными щеками, с помощью которых он способен вырабатывать электричество. Кроме того, имеет кисти, на каждой из которых по пять пальцев, хотя на ногах их всего по три. На спинке виднеются интересные полосатые полосочки коричневого цвета. Еще одной отличительной особенностью покемона является Z-образной формы хвост, напоминающий по внешнему виду разряд молнии.",
                                avatar = "http://pokemondb.ru/images/pokemon/25.png",
                                element = Element.ElectricElement()
                            ),
                            Pokemon(
                                name = "Raichu",
                                avatar = "http://pokemondb.ru/images/pokemon/26.png",
                                element = Element.ElectricElement()
                            ),
                            Pokemon(
                                name = "Magnemite",
                                avatar = "http://pokemondb.ru/images/pokemon/81.png",
                                element = Element.ElectricElement()
                            ),
                            Pokemon(
                                name = "Magneton",
                                avatar = "http://pokemondb.ru/images/pokemon/82.png",
                                element = Element.ElectricElement()
                            ),
                            Pokemon(
                                name = "Voltorb",
                                avatar = "http://pokemondb.ru/images/pokemon/100.png",
                                element = Element.ElectricElement()
                            ),
                            Pokemon(
                                name = "Charmander",
                                desc = "Charmander (он же Чармандер) является первым покемоном из категории огненных, который также обитает в Канто. По своему облику имеет схожесть с огненной саламандрой. Кроме того, в Pokemon Go ему присуще некоторые качества динозавра-теропода.\n" +
                                        "\n" +
                                        "Окраска почти всего тела покемона – оранжевого цвета, однако брюшко светло-желтое. На самом кончике хвоста у него всегда горит вечное пламя, причем известно, что оно есть у покемона с самого его появления на свет. Ходят слухи, что если его наконечное пламя случайно потухнет, то сам покемон просто-напросто погибает. Проживает Чармандер, как правило, в горной местности вблизи вулканов.",
                                avatar = "http://pokemondb.ru/images/pokemon/4.png",
                                element = Element.FireElement()
                            ), Pokemon(
                                name = "Charmeleon",
                                desc = "Charmeleon (он же Чармилион) – это покемон из категории огненных, представляющий собой вторую стадию эволюции Чармандера. Отличается почти вдвое увеличенными габаритами: 19 килограммов веса при росте в 110 сантиметров.\n" +
                                        "\n" +
                                        "По внешнему виду выглядит темнее своего предшественника, а также получил на голове необычный гребень, который напоминает облик некоторых дизозавров-гадрозаврид. Кроме того, преобразились и лапы, где вместо пальцев у покемона теперь достаточно крупные когти. На хвосте также горит столь важный для покемона огонь, являющийся его отличительной особенностью.",
                                avatar = "http://pokemondb.ru/images/pokemon/5.png",
                                element = Element.FireElement()
                            ),
                            Pokemon(
                                name = "Charizard",
                                desc = "Charizard (он же Чаризард) – это представитель огненных покемонов, который к тому же умеет летать. Этот представитель Pokemon Go является финальной стадией эволюции Чармандера и предыдущей стадии Чармелиона.\n" +
                                        "\n" +
                                        "По внешнему облику Чаризард достаточно сильно отличается от своих предшественников. В первую очередь благодаря тому, что у него имеются крылья, поэтому он свободно умеет летать. В то же время на хвосту также располагается огонь. Передвигается Чаризард на двух лапах, но при этом располагает несколько удлиненными частями тела. При росте в 160 сантиметров, весит покемон чуть более 90 килограммов.",
                                avatar = "http://pokemondb.ru/images/pokemon/6.png",
                                element = Element.FireElement()
                            ),
                            Pokemon(
                                name = "Vulpix",
                                avatar = "http://pokemondb.ru/images/pokemon/37.png",
                                element = Element.FireElement()
                            ),
                            Pokemon(
                                name = "Ninetales",
                                avatar = "http://pokemondb.ru/images/pokemon/38.png",
                                element = Element.FireElement()
                            ),
                            Pokemon(
                                name = "Squirtle",
                                desc = "Squirtle (Сквиртл) является первым покемонов из водной серии, который также представляет Канто-регион. Его отличительная особенность заключена в наличии панциря, из-за которого он очень напоминает черепаху.\n" +
                                        "\n" +
                                        "Как правило, в Pokemon Go Сквиртл передвигается на двух лапах, хотя изредка также перемещается на четвереньках. Наличие панциря обеспечивает отличную защиту. При необходимости покемон может легко в нем скрыться. А атаковать соперника может с наибольшей эффективностью благодаря наличию маленького клыка, который он использует только при нападении. Кроме того, они нередко атакуют, используя свой хвост.",
                                avatar = "http://pokemondb.ru/images/pokemon/7.png",
                                element = Element.WaterElement()
                            ),
                            Pokemon(
                                name = "Wartortle",
                                desc = "Wartortle (Вартортл) – это покемон из водной серии, который является второй стадией эволюции Сквритла. В отличие от своего предшественника, он имеет увеличенные размеры, цвет его кожи стал темнее и теперь ближе к синему, а хвост и уши более пушистые.\n" +
                                        "\n" +
                                        "Примечательно, что в Pokemon Go у Вартортла уже два клыка. Есть традиционный панцирь, в который покемон прячется при необходимости обороняться. На лапах появились когти вместо пальцев. При необходимости он также использует в атаке не только клыки, но и хвост. С помощью этой части тела он может выполнять сразу несколько сокрушительных ударов.",
                                avatar = "http://pokemondb.ru/images/pokemon/8.png",
                                element = Element.WaterElement()
                            ),
                            Pokemon(
                                name = "Blastoise",
                                desc = "Blastoise (Бластойз) – это третий покемон из водяной категории, являющийся завершительной стадией развития Сквиртла и следующей стадией развития Вартотла. В Pokemon Go он выглядит как очень большая по своим размерам 2-ногая черепаха синего цвета.\n" +
                                        "\n" +
                                        "Отличительной особенностью во внешнем виде Бластойза является очень жесткий и надежный панцирь коричневого или зеленого цвета. Причем из самого панциря с правой и левой сторон торчат две огромные пушки. Сам по себе покемон отличается ростом в 160 см и весит чуть более 85 килограммов. При необходимости может спрятать пушки под панцирь, как и конечности.",
                                avatar = "http://pokemondb.ru/images/pokemon/9.png",
                                element = Element.WaterElement()
                            ),
                            Pokemon(
                                name = "Psyduck",
                                avatar = "http://pokemondb.ru/images/pokemon/54.png",
                                element = Element.WaterElement()
                            ),
                            Pokemon(
                                name = "Golduck",
                                avatar = "http://pokemondb.ru/images/pokemon/55.png",
                                element = Element.WaterElement()
                            ),
                            Pokemon(
                                name = "Dewgong",
                                avatar = "http://pokemondb.ru/images/pokemon/87.png",
                                element = Element.IceElement()
                            ),
                            Pokemon(
                                name = "Cloyster",
                                avatar = "http://pokemondb.ru/images/pokemon/91.png",
                                element = Element.IceElement()
                            ),
                            Pokemon(
                                name = "Jynx",
                                avatar = "http://pokemondb.ru/images/pokemon/124.png",
                                element = Element.IceElement()
                            ),
                            Pokemon(
                                name = "Lapras",
                                avatar = "http://pokemondb.ru/images/pokemon/131.png",
                                element = Element.IceElement()
                            ),
                            Pokemon(
                                name = "Articuno",
                                avatar = "http://pokemondb.ru/images/pokemon/144.png",
                                element = Element.IceElement()
                            ),
                            Pokemon(
                                name = "Sandshrew",
                                desc = "Sandshrew (он же Сэндшрю) входит в категорию покемонов земляного типа. После скормления 50 конфет превращается в Сэндслэша.\n" +
                                        "По внешнему виду в PokemonGo Сэндшрю выглядит как очень необычный зверек – панголин. Обладая достаточно жестким телом, его окраска отличается песочным цветом. Вполне возможно, что такая окраска выбрана не случайно, а в целях более эффективной маскировки под фон земли. При необходимости покемон может легко свернутся в шарик и защитить свой живот, который у него является очень мягким. Главным оружием его стали очень маленькие, но мощные когти на лапах.",
                                avatar = "http://pokemondb.ru/images/pokemon/27.png",
                                element = Element.GroundElement()
                            ),
                            Pokemon(
                                name = "Sandslash",
                                desc = "Sandslash (Сэндслэш) – это покемон из категории земляных, который является процессом эволюции Сэндшрю. При этом по внешнему виду достаточно сильно отличается от предшественника.\n" +
                                        "В Pokemon Go его отличает более массивная шкура на спине, которая также снабжена острыми шипами коричневого цвета. Он точно также способен сворачиваться в целях защитить свое мягкое брюшко. Причем в таком скрученном положении покемон может с большой скоростью катиться в сторону врагов, атакуя их своими шипами на спине. Также отличным оружием выступают когти на всех четырех лапах. Правда, они иногда ломаются, но вместо них почти сразу вырастают новые.",
                                avatar = "http://pokemondb.ru/images/pokemon/28.png",
                                element = Element.GroundElement()
                            ),
                            Pokemon(
                                name = "Nidoqueen",
                                avatar = "http://pokemondb.ru/images/pokemon/31.png",
                                element = Element.GroundElement()
                            ),
                            Pokemon(
                                name = "Nidoking",
                                desc = "Nidoking (Нидокинг) – это покемон из категории земляного/ядовитого типа, представляющий собой завершающий этап эволюции из Нидоран.\n" +
                                        "По своим габаритам в Pokemon Go Нидокинг сильно отличается от своих предшественников. В первую очередь благодаря возможности устойчиво стоять и передвигаться на двух задних лапах. Рог на голове при этом стал ощутимо длинее, как и уши. Любопытно, что покемон отныне может похвастать наличием 20 зубов – по 10 на каждую челюсть. А на спине располагается множество ядовитых шипов. Хвост в то же время является бронированным. При росте в 140 см весит этот покемон порядка 62 кг.",
                                avatar = "http://pokemondb.ru/images/pokemon/34.png",
                                element = Element.GroundElement()
                            ),
                            Pokemon(
                                name = "Diglett",
                                avatar = "http://pokemondb.ru/images/pokemon/50.png",
                                element = Element.GroundElement()
                            )
                        )
                    )
                })
        }
    }
}
