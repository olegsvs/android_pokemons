package ru.olegsvs.pokemons.data.model.pokemon

import java.io.Serializable

sealed class Element() : Serializable {
    abstract val elementName: String

    data class ElectricElement(override val elementName: String = "Electric") : Element()
    data class FireElement(override val elementName: String = "Fire") : Element()
    data class GroundElement(override val elementName: String = "Ground") : Element()
    data class IceElement(override val elementName: String = "Ice") : Element()
    data class WaterElement(override val elementName: String = "Water") : Element()
    data class NoneElement(override val elementName: String = "None") : Element()
}

