package ru.olegsvs.pokemons.data.dao

import androidx.room.TypeConverter
import ru.olegsvs.pokemons.data.model.pokemon.Element

class ElementConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun sealedClassToString(sealedClass: Element) : String = sealedClass.elementName

        @JvmStatic
        @TypeConverter
        fun sealedClassFromString(elementName: String) : Element = elementName.let {
            return when(it) {
                "Fire" -> Element.FireElement()
                "Ice" -> Element.IceElement()
                "Ground" -> Element.GroundElement()
                "Water" -> Element.WaterElement()
                "Electric" -> Element.ElectricElement()
                else -> Element.NoneElement()
            }
        }
    }
}