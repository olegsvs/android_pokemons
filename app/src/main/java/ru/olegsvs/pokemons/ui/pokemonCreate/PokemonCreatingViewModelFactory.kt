package ru.olegsvs.pokemons.ui.pokemonCreate

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.olegsvs.pokemons.data.dao.PokemonDao

class PokemonCreatingViewModelFactory(
    private val dataSource: PokemonDao? = null
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PokemonCreatingViewModel::class.java)) {
            return PokemonCreatingViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}