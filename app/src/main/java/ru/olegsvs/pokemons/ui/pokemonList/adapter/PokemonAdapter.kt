package ru.olegsvs.pokemons.ui.pokemonList.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.olegsvs.pokemons.common.BaseItem
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import ru.olegsvs.pokemons.databinding.ElementHeaderBinding
import ru.olegsvs.pokemons.databinding.PokemonBinding

const val HEADER_TYPE = 0
const val POKEMON_TYPE = 1
typealias OnPokemonItemClickListener = (pokemon: Pokemon?) -> Unit

class PokemonAdapter(
    private val mOnItemClickListener: OnPokemonItemClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val pokemons: MutableList<BaseItem> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (viewType.equals(HEADER_TYPE)) {
            val binding: ElementHeaderBinding =
                ElementHeaderBinding.inflate(inflater, parent, false)
            return HeaderHolder(
                binding
            )
        } else {
            val binding: PokemonBinding = PokemonBinding.inflate(inflater, parent, false)
            return PokemonHolder(
                binding, mOnItemClickListener
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PokemonHolder -> {
                val pokemon: Pokemon? = pokemons.get(position) as Pokemon
                pokemon?.let {
                    holder.bind(it)
                }
            }
            is HeaderHolder -> {
                val header: Header? = pokemons.get(position) as Header
                header?.let {
                    holder.bind(it)
                }
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (pokemons.get(position)) {
            is Pokemon -> POKEMON_TYPE
            is Header -> HEADER_TYPE
            else -> -1
        }
    }


    override fun getItemCount(): Int = pokemons.size

    fun submitList(data: List<Pokemon>?) {
        pokemons.clear()
        data?.let {
            data
                //.sortedBy { it.element } sort in DB
                .groupBy { it.element }.mapValues {
                    pokemons.add(Header(it.key))
                    pokemons.addAll(it.value.sortedBy { it.name })
                }
        }
    }

}
