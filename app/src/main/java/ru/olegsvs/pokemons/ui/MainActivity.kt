package ru.olegsvs.pokemons.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import ru.olegsvs.pokemons.R
import ru.olegsvs.pokemons.ui.pokemonList.PokemonListFragment

class MainActivity : AppCompatActivity(), FragmentManager.OnBackStackChangedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        supportFragmentManager.addOnBackStackChangedListener(this);
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container,
                    PokemonListFragment.newInstance(),
                    PokemonListFragment::class.java.name
                )
                .commitNow()
        }
        shouldDisplayHomeUp();
    }

    fun shouldDisplayHomeUp() {
        val canGoBack = supportFragmentManager.backStackEntryCount > 0
        supportActionBar!!.setDisplayHomeAsUpEnabled(canGoBack)
    }

    override fun onSupportNavigateUp(): Boolean {
        supportFragmentManager.popBackStack()
        return true
    }

    override fun onBackPressed() {
        val fragmentManager: FragmentManager = supportFragmentManager
        if (fragmentManager.getBackStackEntryCount() == 0) {
            finish()
        } else {
            fragmentManager.popBackStack()
        }
    }

    override fun onBackStackChanged() {
        shouldDisplayHomeUp();
    }
}