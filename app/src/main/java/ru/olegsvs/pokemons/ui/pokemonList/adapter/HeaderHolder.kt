package ru.olegsvs.pokemons.ui.pokemonList.adapter

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import ru.olegsvs.pokemons.databinding.ElementHeaderBinding

class HeaderHolder(binding: ElementHeaderBinding) : ViewHolder(binding.getRoot()) {
    private val elementHeaderBinding: ElementHeaderBinding
    fun bind(item: Header) {
        elementHeaderBinding.vm = HeaderItemViewModel(item)
        elementHeaderBinding.executePendingBindings()
    }

    init {
        elementHeaderBinding = binding
    }
}
