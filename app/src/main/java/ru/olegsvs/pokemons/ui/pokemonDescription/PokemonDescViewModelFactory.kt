package ru.olegsvs.pokemons.ui.pokemonDescription

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon

class PokemonDescViewModelFactory(private val pokemon: Pokemon? = null) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PokemonDescriptionViewModel::class.java)) {
            return PokemonDescriptionViewModel(pokemon) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}