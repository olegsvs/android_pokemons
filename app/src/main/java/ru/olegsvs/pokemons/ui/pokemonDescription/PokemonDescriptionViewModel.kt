package ru.olegsvs.pokemons.ui.pokemonDescription

import androidx.lifecycle.ViewModel
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon

class PokemonDescriptionViewModel(
    val pokemon: Pokemon?
) : ViewModel() {
}