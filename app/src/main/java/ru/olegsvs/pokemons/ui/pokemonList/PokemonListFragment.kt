package ru.olegsvs.pokemons.ui.pokemonList

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fr_pokemon_list_fragment.*
import ru.olegsvs.pokemons.Injection
import ru.olegsvs.pokemons.R
import ru.olegsvs.pokemons.databinding.PokemonsBinding
import ru.olegsvs.pokemons.ui.pokemonCreate.PokemonCreatingFragment
import ru.olegsvs.pokemons.ui.pokemonDescription.PokemonDescriptionFragment


class PokemonListFragment : Fragment() {

    companion object {
        fun newInstance() = PokemonListFragment()
    }

    private lateinit var viewModel: PokemonListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: PokemonsBinding = PokemonsBinding.inflate(inflater, container, false)
        binding.setVm(viewModel)
        binding.setLifecycleOwner(viewLifecycleOwner)
        return binding.getRoot()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> fab.show()
                    else -> fab.hide()
                }
                super.onScrollStateChanged(recyclerView, newState)
            }

        }
        viewModel.showPokemonCreationScreen.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let {
                showFragment(
                    PokemonCreatingFragment.newInstance(),
                    PokemonListFragment::class.java.name
                )
            }
        })
        viewModel.showPokemonDetailsScreen.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let {
                showFragment(
                    PokemonDescriptionFragment.newInstance(it),
                    PokemonListFragment::class.java.name
                )
            }
        })
        pokemons_recycler.clearOnScrollListeners()
        pokemons_recycler.addOnScrollListener(scrollListener)
    }

    private fun showFragment(newInstance: Fragment, tag: String) {
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.container, newInstance)
            ?.addToBackStack(tag)?.commit()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelProviders.of(
            this, Injection.providePokemonListViewModelFactory(requireContext()) { pokemon ->
                viewModel.onPokemonClick(pokemon)
            }
        )
            .get(PokemonListViewModel::class.java)
    }
}