package ru.olegsvs.pokemons.ui.pokemonList.adapter

import ru.olegsvs.pokemons.common.BaseItem
import ru.olegsvs.pokemons.data.model.pokemon.Element

data class Header(
    val element: Element
) : BaseItem()


