package ru.olegsvs.pokemons.ui.pokemonList.adapter

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import ru.olegsvs.pokemons.databinding.PokemonBinding

class PokemonHolder(binding: PokemonBinding, val listener: OnPokemonItemClickListener) :
    ViewHolder(binding.getRoot()) {
    private val pokemonBinding: PokemonBinding
    fun bind(item: Pokemon) {
        pokemonBinding.apply {
            vm = PokemonListItemViewModel(item)
            onPokemonItemClickListener = listener
            executePendingBindings()
        }

    }

    init {
        pokemonBinding = binding
    }
}
