package ru.olegsvs.pokemons.ui.pokemonDescription

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ru.olegsvs.pokemons.Injection
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import ru.olegsvs.pokemons.databinding.PokemonDescBinding

class PokemonDescriptionFragment : Fragment() {
    private lateinit var pokemon: Pokemon

    companion object {
        @JvmStatic
        fun newInstance(pokemon: Pokemon?) = PokemonDescriptionFragment().apply {
            pokemon?.let {
                arguments = Bundle().apply {
                    putSerializable("pokemon", pokemon)
                }
            }
        }
    }

    private lateinit var viewModel: PokemonDescriptionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: PokemonDescBinding = PokemonDescBinding.inflate(inflater, container, false)
        binding.setVm(viewModel)
        binding.setLifecycleOwner(this)
        return binding.getRoot()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getSerializable("pokemon")?.let {
            pokemon = it as Pokemon
        }
        viewModel = ViewModelProviders.of(
            this,
            Injection.providePokemonDescriptionViewModel(pokemon)
        )
            .get(PokemonDescriptionViewModel::class.java)
    }
}