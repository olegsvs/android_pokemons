package ru.olegsvs.pokemons.ui.pokemonList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.olegsvs.pokemons.data.dao.PokemonDao
import ru.olegsvs.pokemons.ui.pokemonList.adapter.OnPokemonItemClickListener
import ru.olegsvs.pokemons.ui.pokemonList.adapter.PokemonAdapter

class PokemonListViewModelFactory(
    private val dataSource: PokemonDao? = null,
    private val listener: OnPokemonItemClickListener? = null
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PokemonListViewModel::class.java)) {
            return PokemonListViewModel(dataSource, listener) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}