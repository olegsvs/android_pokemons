package ru.olegsvs.pokemons.ui.pokemonCreate

import android.widget.RadioGroup
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.olegsvs.pokemons.R
import ru.olegsvs.pokemons.data.dao.PokemonDao
import ru.olegsvs.pokemons.data.model.pokemon.Element
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import ru.olegsvs.pokemons.utils.Event
import java.util.concurrent.Executors


class PokemonCreatingViewModel(
    private val dataSource: PokemonDao?
) : ViewModel() {
    var pokemonName: ObservableField<String> = ObservableField("")
    var pokemonDescription: ObservableField<String> = ObservableField("")
    var pokemonAvatarURL: ObservableField<String> = ObservableField("")
    var pokemonElement: Element = Element.WaterElement()
    val pokemonAdded: MutableLiveData<Event<String>> = MutableLiveData<Event<String>>()

    fun onCreateClick() {
        Executors.newSingleThreadScheduledExecutor()
            .execute(Runnable {
                val pokemon: Pokemon = Pokemon(
                    name = pokemonName.get() ?: "",
                    desc = pokemonDescription.get() ?: "",
                    avatar = pokemonAvatarURL.get() ?: "",
                    element = pokemonElement
                )
                dataSource?.insertPokemon(pokemon)
                pokemonAdded.postValue(Event(pokemon.name))
            })
    }

    fun onElementTypeChanged(id: Int) {
        when (id) {
            R.id.element_electric -> pokemonElement = Element.ElectricElement()
            R.id.element_water -> pokemonElement = Element.WaterElement()
            R.id.element_fire -> pokemonElement = Element.FireElement()
            R.id.element_ground -> pokemonElement = Element.GroundElement()
            R.id.element_ice -> pokemonElement = Element.IceElement()
        }
    }
}