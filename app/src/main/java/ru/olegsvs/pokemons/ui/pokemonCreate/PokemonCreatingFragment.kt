package ru.olegsvs.pokemons.ui.pokemonCreate

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ru.olegsvs.pokemons.Injection
import ru.olegsvs.pokemons.databinding.PokemonCreationBinding

class PokemonCreatingFragment : Fragment() {
    companion object {
        @JvmStatic
        fun newInstance() = PokemonCreatingFragment()
    }

    private lateinit var viewModel: PokemonCreatingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: PokemonCreationBinding =
            PokemonCreationBinding.inflate(inflater, container, false)
        binding.setVm(viewModel)
        binding.setLifecycleOwner(this)
        return binding.getRoot()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.pokemonAdded.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let {
                Toast.makeText(requireContext(), "Pokemon \"$it\" added!", Toast.LENGTH_LONG).show()
                fragmentManager?.popBackStack()
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelProviders.of(
            this,
            Injection.providePokemonCreationViewModelFactory(context)
        )
            .get(PokemonCreatingViewModel::class.java)
    }
}