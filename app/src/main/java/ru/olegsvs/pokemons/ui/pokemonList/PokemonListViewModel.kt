package ru.olegsvs.pokemons.ui.pokemonList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.olegsvs.pokemons.data.dao.PokemonDao
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import ru.olegsvs.pokemons.ui.pokemonList.adapter.OnPokemonItemClickListener
import ru.olegsvs.pokemons.ui.pokemonList.adapter.PokemonAdapter
import ru.olegsvs.pokemons.utils.Event


class PokemonListViewModel(
    private val dataSource: PokemonDao?,
    val listener: OnPokemonItemClickListener?
) : ViewModel() {
    val showPokemonCreationScreen: MutableLiveData<Event<Unit>> = MutableLiveData<Event<Unit>>()
    val showPokemonDetailsScreen: MutableLiveData<Event<Pokemon?>> = MutableLiveData<Event<Pokemon?>>()

    private val pokemons: LiveData<List<Pokemon>> = dataSource?.getPokemonsLive()!!

    fun getPokemons(): LiveData<List<Pokemon>> {
        return pokemons
    }

    fun onFabClick() {
        showPokemonCreationScreen.postValue(Event(Unit))
    }

    fun onPokemonClick(pokemon : Pokemon?) {
        showPokemonDetailsScreen.postValue(Event(pokemon))
    }
}