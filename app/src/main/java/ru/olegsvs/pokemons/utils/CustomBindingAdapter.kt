package ru.olegsvs.pokemons.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ru.olegsvs.pokemons.R
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import ru.olegsvs.pokemons.ui.pokemonList.adapter.OnPokemonItemClickListener
import ru.olegsvs.pokemons.ui.pokemonList.adapter.PokemonAdapter


object CustomBindingAdapter {
    @JvmStatic
    @BindingAdapter("bind:imageUrl")
    fun loadImage(imageView: ImageView, urlImage: String?) {
        if (!urlImage.isNullOrEmpty()) {
            Picasso.get().load(urlImage).error(R.drawable.placeholder).fit().centerCrop().into(imageView)
        } else {
            Picasso.get().load(R.drawable.placeholder).fit().centerCrop().into(imageView)
        }
    }

    @JvmStatic
    @BindingAdapter("bind:data", "bind:clickHandler")
    fun configureRecyclerView(
        recyclerView: RecyclerView,
        projects: List<Pokemon>?,
        listener: OnPokemonItemClickListener?
    ) {
        val adapter = PokemonAdapter(listener!!)
        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        adapter.submitList(projects)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = adapter
    }
}