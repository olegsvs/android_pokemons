package ru.olegsvs.pokemons

import android.content.Context
import ru.olegsvs.pokemons.data.dao.PokemonDao
import ru.olegsvs.pokemons.data.dao.PokemonDatabase
import ru.olegsvs.pokemons.data.model.pokemon.Pokemon
import ru.olegsvs.pokemons.ui.pokemonCreate.PokemonCreatingViewModelFactory
import ru.olegsvs.pokemons.ui.pokemonDescription.PokemonDescViewModelFactory
import ru.olegsvs.pokemons.ui.pokemonList.PokemonListViewModelFactory
import ru.olegsvs.pokemons.ui.pokemonList.adapter.OnPokemonItemClickListener
import ru.olegsvs.pokemons.ui.pokemonList.adapter.PokemonAdapter

object Injection {

    fun providePokemonDataSource(context: Context): PokemonDao {
        val database = PokemonDatabase.getInstance(context)
        return database.pokemonDao()
    }

    fun providePokemonListViewModelFactory(
        context: Context,
        listener: OnPokemonItemClickListener
    ): PokemonListViewModelFactory {
        val dataSource = providePokemonDataSource(context)
        return PokemonListViewModelFactory(dataSource, listener)
    }

    fun providePokemonDescriptionViewModel(pokemon: Pokemon): PokemonDescViewModelFactory {
        return PokemonDescViewModelFactory(pokemon)
    }

    fun providePokemonCreationViewModelFactory(context: Context): PokemonCreatingViewModelFactory {
        val dataSource = providePokemonDataSource(context)
        return PokemonCreatingViewModelFactory(dataSource)
    }
}